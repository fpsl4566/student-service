package com.iy.academyapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentRequest {
    public String name;
    public String gender;
    public Byte age;
    public String address;

}
