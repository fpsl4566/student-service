package com.iy.academyapi.controller;

import com.iy.academyapi.model.StudentRequest;
import com.iy.academyapi.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("student")

public class StudentController {
    private final StudentService studentService;

    @PostMapping("/new")
    public String setStudent(@RequestBody StudentRequest request) {
        studentService.setStudent(request);
        return "OK";

    }
}
